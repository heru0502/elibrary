<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Book;
use App\Models\Category;
use Illuminate\Support\Facades\Auth;

class BookController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = Book::orderBy('id', 'desc')->paginate(10);
        return view('admin.book.index', compact('books', $books));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Category::all();
        return view('admin.book.create', compact('categories', $categories));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'book_number' => 'required',
            'title' => 'required',
            'description' => 'required',
            'author' => 'required',
            'publish_date' => 'required',
            // 'pdf_file' => 'required',
            // 'thumbnail' => 'required',
        ]);

        $pdf_file = $request->file('pdf_file')->store('pdf_files');
        $thumbnail = $request->file('thumbnail')->store('thumbnails');

        // $book = new Book;
        // $book->book_type = 'elibrary';
        // $book->book_number = $request->book_number;
        // $book->title = $request->title;
        // $book->description = $request->description;
        // $book->author = $request->author;
        // $book->publish_date = $request->publish_date;
        // $book->pdf_file = $pdf_file;
        // $book->thumbnail = $thumbnail;
        // $book->created_by = Auth::id();
        // $book->save();

        $book = Book::create([
            'book_type' => 'elibrary',
            'book_number' => $request->book_number,
            'title' => $request->title,
            'description' => $request->description,
            'author' => $request->author,
            'publish_date' => $request->publish_date,
            'pdf_file' => $pdf_file,
            'thumbnail' => $thumbnail,
            'created_by' => Auth::id(),
        ]);

        $categories = Category::find($request->category);
        // dd($categories);

        $book->categories()->attach($categories);

        return redirect()->route('book.index')->with('success', 'Berhasil disimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $book = Book::find($id);
        $book->delete();

        return back();
    }
}
