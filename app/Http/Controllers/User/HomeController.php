<?php

namespace App\Http\Controllers\User;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Book;

class HomeController extends Controller
{
    public function index()
    {
        $books = Book::orderBy('id', 'desc')->paginate(6);
        return view('user.home.index', compact('books', $books));
    }

    public function show($id)
    {
        $book = Book::find($id);
        return view('user.home.show', [
            'book' => $book
        ]);
    }
}
