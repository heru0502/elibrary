<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Book extends Model
{
    protected $fillable = [
        'book_type',
        'book_number',
        'title',
        'description',
        'author',
        'publish_date',
        'pdf_file',
        'thumbnail',
        'created_by'
    ];

    public function categories()
    {
        return $this->belongsToMany(Category::class);
    }
}
