<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $categories = [
            [
                'name' => 'Romance'
            ],
            [
                'name' => 'Mystery & Thriller'
            ],
            [
                'name' => 'Science Fiction'
            ],
        ];

        foreach($categories as $category) {
            DB::table('categories')->insert($category);
        }
    }
}
