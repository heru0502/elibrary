<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $users = [
            [
                'name' => 'Heru Firmansyah',
                'email' => 'heru0502@gmail.com',
                'password' => bcrypt('rahasia'),
            ],
            [
                'name' => 'Tri Wahyudi Noor',
                'email' => 'duniayhudi@gmail.com',
                'password' => bcrypt('rahasia'),
            ]  
        ];

        foreach($users as $user) {
            DB::table('users')->insert($user);
        }

    }
}
