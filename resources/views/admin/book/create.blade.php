@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Form</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <form action="{{ route('book.store') }}" method="post" enctype="multipart/form-data">
                        @csrf
                        <div class="card-header">
                            <h4>Form</h4>
                        </div>
                        <div class="card-body">
                    
                            <div class="form-group row">
                                <label for="book_number" class="col-sm-3 col-form-label">Nomor Buku</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control @error('book_number') is-invalid @enderror" id="book_number" name="book_number" value="{{ old('book_number') }}">
                                    @error('book_number')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="title" class="col-sm-3 col-form-label">Judul</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control @error('title') is-invalid @enderror" id="title" name="title" value="{{ old('title') }}">
                                    @error('title')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="description" class="col-sm-3 col-form-label">Deskripsi</label>
                                <div class="col-sm-9">
                                    <textarea class="summernote-simple @error('description') is-invalid @enderror" name="description">{{ old('description') }}</textarea>
                                    @error('description')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="author" class="col-sm-3 col-form-label">Pengarang</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control @error('author') is-invalid @enderror" id="author" name="author" value="{{ old('author') }}">
                                    @error('author')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="publish_date" class="col-sm-3 col-form-label">Tanggal Terbit</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control datepicker @error('publish_date') is-invalid @enderror" name="publish_date" value="{{ old('publish_date') }}">
                                    @error('publish_date')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="publish_date" class="col-sm-3 col-form-label">Kategori</label>
                                <div class="col-sm-9">
                                    <div class="selectgroup selectgroup-pills">
                                        @foreach ($categories as $category)
                                            <label class="selectgroup-item">
                                                <input type="checkbox" name="category[]" value="{{ $category }}" class="selectgroup-input">
                                                <span class="selectgroup-button">{{ $category->name }}</span>
                                            </label>
                                        @endforeach
                                        
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="pdf_file" class="col-sm-3 col-form-label">File Ebook</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control @error('pdf_file') is-invalid @enderror" id="pdf_file" name="pdf_file" value="{{ old('pdf_file') }}" accept=".pdf">
                                    @error('pdf_file')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row">
                                <label for="thumbnail" class="col-sm-3 col-form-label">Cover</label>
                                <div class="col-sm-9">
                                    <input type="file" class="form-control @error('thumbnail') is-invalid @enderror" id="thumbnail" name="thumbnail" value="{{ old('thumbnail') }}" accept=".jpg,.png">
                                    @error('thumbnail')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                    
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                            <a href="{{ route('users.index') }}" class="btn btn-secondary">Batal</a>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection

@push('styles')
<link rel="stylesheet" href="{{ asset('stisla/node_modules/summernote/dist/summernote-bs4.css') }}">
<link rel="stylesheet" href="{{ asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.css') }}">
@endpush

@push('scripts')
<script src="{{ asset('stisla/node_modules/summernote/dist/summernote-bs4.js') }}"></script>
<script src="{{ asset('stisla/node_modules/bootstrap-daterangepicker/daterangepicker.js') }}"></script>
@endpush