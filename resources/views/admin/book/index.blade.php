@extends('admin.templates.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Buku</h1>
        </div>
        
        @include('admin.templates.partials._alerts')

        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Daftar Buku</h4>
                        </div>
                        <div class="card-body p-0">
                            <div class="col-sm-12">
                                <a href="{{ route('book.create') }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-plus"></i> Tambah</a>
                            </div>
                        </div>
                        <div class="card-body p-1">
                            <div class="table-responsive">
                                <table class="table table-striped table-md">
                                    <tr>
                                        <th>#</th>
                                        <th>Nomor Buku</th>
                                        <th>Judul</th>
                                        <th>Pengarang</th>
                                        <th>Tanggal Terbit</th>
                                        <th>Cover</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach ($books as $key => $book)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $book->book_number }}</td>
                                        <td>{{ $book->title }}</td>
                                        <td>{{ $book->author }}</td>
                                        <td>{{ $book->publish_date }}</td>
                                        <td><img src="{{ asset('storage/'.$book->thumbnail) }}" height="100"></td>
                                        <td>
                                            <form action="{{ route('book.destroy', $book) }}" method="post">
                                                @csrf
                                                @method("DELETE")
                                                
                                                <a href="{{ route('book.edit', $book) }}" class="btn btn-info btn-icon icon-left"><i class="fas fa-edit"></i> Ubah</a>
                                                <button type="submit" class="btn btn-danger btn-icon icon-left"><i class="fas fa-trash"></i> Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <nav class="d-inline-block">
                                {{ $books->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </section>
@endsection
