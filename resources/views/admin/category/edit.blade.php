@extends('admin.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
        <h1>Form</h1>
    </div>

    <div class="section-body">
        <div class="row">
            <div class="col-12 col-md-12 col-lg-12">
                <div class="card">
                    <form action="{{ route('category.update', $category) }}" method="post">
                        @csrf
                        @method("PUT")
                        <div class="card-header">
                            <h4>Form</h4>
                        </div>
                        <div class="card-body">
                    
                            <div class="form-group row">
                                <label for="name" class="col-sm-3 col-form-label">Nama Kategori</label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control @error('name') is-invalid @enderror" id="name" name="name" value="{{ $category->name ?? old('name') }}">
                                    @error('name')
                                        <div class="invalid-feedback">
                                            {{ $message }}
                                        </div>
                                    @enderror
                                </div>
                            </div>
                    
                        </div>
                        <div class="card-footer text-right">
                            <button class="btn btn-primary mr-1" type="submit">Simpan</button>
                            <a href="{{ route('category.index') }}" class="btn btn-secondary">Batal</a>
                        </div>
                    </form>    
                </div>
            </div>
        </div>
    </div>
</section>
@endsection