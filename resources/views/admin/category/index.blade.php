@extends('admin.templates.default')

@section('content')

    <section class="section">
        <div class="section-header">
            <h1>Category</h1>
        </div>
        
        @include('admin.templates.partials._alerts')

        <div class="section-body">
            <div class="row">
                <div class="col-12 col-md-12 col-lg-12">
                    <div class="card">
                        <div class="card-header">
                            <h4>Full Width</h4>
                        </div>
                        <div class="card-body p-0">
                            <div class="col-sm-12">
                                <a href="{{ route('category.create') }}" class="btn btn-primary btn-icon icon-left"><i class="fas fa-plus"></i> Tambah</a>
                            </div>
                        </div>
                        <div class="card-body p-1">
                            <div class="table-responsive">
                                <table class="table table-striped table-md">
                                    <tr>
                                        <th>#</th>
                                        <th>Name</th>
                                        <th>Action</th>
                                    </tr>
                                    @foreach ($categories as $key => $category)
                                    <tr>
                                        <td>{{ $key+1 }}</td>
                                        <td>{{ $category->name }}</td>
                                        <td>
                                            <form action="{{ route('category.destroy', $category) }}" method="post">
                                                @csrf
                                                @method("DELETE")
                                                
                                                <a href="{{ route('category.edit', $category) }}" class="btn btn-info btn-icon icon-left"><i class="fas fa-edit"></i> Ubah</a>
                                                <button type="submit" class="btn btn-danger btn-icon icon-left"><i class="fas fa-trash"></i> Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </table>
                            </div>
                        </div>
                        <div class="card-footer text-right">
                            <nav class="d-inline-block">
                                {{ $categories->links() }}
                            </nav>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     </section>
@endsection
