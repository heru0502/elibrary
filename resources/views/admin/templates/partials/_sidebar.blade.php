<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand">
            <a href="index.html">{{ config('app.name') }}</a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="index.html">St</a>
        </div>
        <ul class="sidebar-menu">
            <li><a class="nav-link" href="{{ route('dashboard') }}"><i class="fas fa-fire"></i> <span>Dashboard</span></a></li>  
            <li><a class="nav-link" href="{{ route('users.index') }}"><i class="fas fa-user"></i> <span>User</span></a></li>  
            <li><a class="nav-link" href="blank.html"><i class="fas fa-landmark"></i> <span>Pelayanan</span></a></li>  
            <li><a class="nav-link" href="{{ route('category.index') }}"><i class="fas fa-tag"></i> <span>Kategori</span></a></li>  
            <li><a class="nav-link" href="{{ route('book.index') }}"><i class="fas fa-book"></i> <span>Buku</span></a></li>        
        </ul>
    </aside>
</div>