@extends('user.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
      <h1>Navigasi</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active">Beranda</a></div>
      </div>
    </div>

    <div class="section-body">
      <h2 class="section-title">Beranda</h2>
      <p class="section-lead">Halaman yang berisi koleksi buku terbaru.</p>

      <div class="row">
        @foreach ($books as $book)
            <div class="col-12 col-sm-6 col-md-6 col-lg-4">
                <article class="article article-style-b">
                  <div class="article-header">
                    <div class="article-image" data-background="{{ asset('storage/' . $book->thumbnail) }}">
                    </div>
                  </div>
                  <div class="article-details">
                    <div class="article-title">
                      <h2><a href="#">{{ $book->title }}</a></h2>
                    </div>
                    <p>{!! Str::limit(strip_tags($book->description), 80) !!}</p>
                    <div class="article-cta">
                      <a href="{{ asset('storage/' . $book->pdf_file) }}" target="_blank" class="btn btn-primary">Baca <i class="fas fa-book-open"></i></a>
                      <a href="{{ route('home.show', $book) }}" class="btn btn-info">Detail <i class="fas fa-chevron-right"></i></a>
                    </div>
                  </div>
                </article>
              </div>
        @endforeach        
      </div>

      {{ $books->links() }}
      
    </div>
  </section>
@endsection