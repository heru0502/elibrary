@extends('user.templates.default')

@section('content')
<section class="section">
    <div class="section-header">
      <h1>Navigasi</h1>
      <div class="section-header-breadcrumb">
        <div class="breadcrumb-item active"><a href="#">Beranda</a></div>
        <div class="breadcrumb-item">Detail Buku</div>
      </div>
    </div>

    <div class="section-body">
      <h2 class="section-title">Detail Buku</h2>
      <p class="section-lead">Halaman ini berisi informasi buku terkait secara lebih detail</p>

      <div class="row">
            <div class="col-md-12">
              <div class="card">
                <div class="card-header">
                  <h4>Buku</h4>
                </div>
                <div class="card-body">
                  <div class="tickets">
                    <div class="ticket-items" id="ticket-items">
                        <img class="img-fluid" src="{{ asset('storage/' . $book->thumbnail) }}">
                    </div>
                    <div class="ticket-content">
                      <div class="ticket-header">
                        <div class="ticket-detail">
                          <div class="ticket-title">
                            <h4>{{ $book->title }}</h4>
                          </div>
                          <div class="ticket-info">
                            <div class="font-weight-600">{{ $book->author }}</div>
                            <div class="bullet"></div>
                            <div class="text-primary font-weight-600">{{ $book->publish_date }}</div>
                          </div>
                        </div>
                      </div>
                      <div class="ticket-description">
                        {!! $book->description !!}

                        <div class="ticket-divider"></div>

                        
                        <div class="ticket-form">
                            <div class="table-responsive">
                                <table class="table table-striped table-hover table-md">
                                    <tr>
                                        <th>No. Buku</th>
                                        <td>{{ $book->book_number }}</td>
                                    </tr>
                                    <tr>
                                        <th>Judul Buku</th>
                                        <td>{{ $book->title }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pengarang</th>
                                        <td>{{ $book->author }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Terbit</th>
                                        <td>{{ $book->publish_date }}</td>
                                    </tr>
                                </table>
                            </div>

                            <div class="form-group text-right">
                                <a href="{{ asset('storage/' . $book->pdf_file) }}" target="_blank" class="btn btn-primary btn-lg"><i class="fas fa-book-open"></i> Baca</a>
                            </div>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
      
    </div>
  </section>
@endsection