<!DOCTYPE html>
<html lang="en">
@include('user.templates.partials._head')

<body class="layout-3">
  <div id="app">
    <div class="main-wrapper container">
      <div class="navbar-bg"></div>
      
      @include('user.templates.partials._navbar')

      <!-- Main Content -->
      <div class="main-content">
        @yield('content')
      </div>
      
      @include('user.templates.partials._footer')
    </div>
  </div>

  @include('user.templates.partials._scripts')

</body>
</html>
