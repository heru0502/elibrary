<nav class="navbar navbar-expand-lg main-navbar">
    <a href="index.html" class="navbar-brand sidebar-gone-hide">{{ config('app.name') }}</a>
    <div class="navbar-nav">
      <a href="#" class="nav-link sidebar-gone-show" data-toggle="sidebar"><i class="fas fa-bars"></i></a>
    </div>
    <div class="nav-collapse">
      <a class="sidebar-gone-show nav-collapse-toggle nav-link" href="#">
        <i class="fas fa-ellipsis-v"></i>
      </a>
    </div>
    <form class="form-inline ml-auto">
      <ul class="navbar-nav">
        <li><a href="#" data-toggle="search" class="nav-link nav-link-lg d-sm-none"><i class="fas fa-search"></i></a></li>
      </ul>
      <div class="search-element">
        <input class="form-control" type="search" placeholder="Search" aria-label="Search" data-width="250">
        <button class="btn" type="submit"><i class="fas fa-search"></i></button>
      </div>
    </form>
    <ul class="navbar-nav navbar-right">
      <li><a href="{{ route('login') }}" class="nav-link nav-link-lg"><i class="far fa-user"></i> Admin</a></li>
    </ul>
  </nav>

  <nav class="navbar navbar-secondary navbar-expand-lg">
    <div class="container">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a href="{{ route('home') }}" class="nav-link"><i class="fas fa-fire"></i><span>Beranda</span></a>
        </li>
        <li class="nav-item">
          <a href="#" class="nav-link"><i class="far fa-heart"></i><span>Pelayanan</span></a>
        </li>
      </ul>
    </div>
  </nav>