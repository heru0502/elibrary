<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'User\HomeController@index')->name('home');
Route::get('/home/{id}', 'User\HomeController@show')->name('home.show');

Auth::routes();

Route::get('/admin/dashboard', 'Admin\DashboardController@index')->name('dashboard');
Route::prefix('admin')->middleware('auth')->namespace('Admin')->group(function () {
    Route::resources([
        'users' => 'UserController',
        'category' => 'CategoryController',
        'book' => 'BookController',
    ]);
});

